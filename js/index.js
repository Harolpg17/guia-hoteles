$(function () {
    var contacto = "";

    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 3000
    });
    $('#contacto').on('show.bs.modal', function(e) {
        console.log('El modal se esta mostrando');
        
        contacto = e.relatedTarget;
        $(e.relatedTarget).removeClass('btn-outline-success');
        $(e.relatedTarget).addClass('btn-primary');
        $(e.relatedTarget).prop('disabled', true);
    });
    $('#contacto').on('shown.bs.modal', function(e) {
        console.log('El modal se mostró');
    });
    $('#contacto').on('hide.bs.modal', function(e) {
        console.log('El modal se oculta');
    });
    $('#contacto').on('hidden.bs.modal', function(e) {
        console.log('El modal se ocultó');                
        $(contacto).prop('disabled', false);
    });
});